<div id="all_posts_div" class="col-md-7" style="padding: 0px 5px;width:50%;<?php echo elgg_get_context() != ('activity' || 'profile') ? 'background-color:#fff' : '' ?>">
    <style>
<?php if (!elgg_is_logged_in()) { ?>
            .elgg-main.elgg-body {
                display: none;
            }
    <?php echo $content; ?>
<?php } else { ?>
            .elgg-sidebar {
                display: none;
            }
<?php } ?>
    </style>
    <?php
    if ((elgg_get_context() == 'activity')) {
        include 'activity.php';
    } elseif ((elgg_get_context() == 'profile')) {
        include 'profile.php';
    } else {
        echo $content;
    }
    ?>
</div>