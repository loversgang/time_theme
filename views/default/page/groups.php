<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get All Groups
//'owner_guid' => $user_guid,
$groups = elgg_get_entities(array(
    'type' => 'group',
        ));

// Latest Groups
$groups_latest = elgg_get_entities(array(
    'type' => 'group',
    'limit' => 10
        ));

// Get Featured Groups
$groups_featured = elgg_get_entities(array(
    'type' => 'group',
    'limit' => 2,
    'order_by' => 'guid desc'
        ));

// Get Featured Groups
$groups_popular = elgg_get_entities(array(
    'type' => 'group',
    'limit' => 3,
        ));
