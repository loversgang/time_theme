<?php

if (elgg_get_context() == 'admin') {
    if (get_input('handler') != 'admin') {
        elgg_deprecated_notice("admin plugins should route through 'admin'.", 1.8);
    }
    _elgg_admin_add_plugin_settings_menu();
    elgg_unregister_css('elgg');
    echo elgg_view('page/admin', $vars);
    return true;
}

// Load Custom CSS
elgg_load_css('font-awesome');
elgg_load_css('slidebars');
elgg_load_css('flowplayer');
elgg_load_css('bootstrap');
elgg_load_css('switcher');
elgg_load_css('chosen');
elgg_load_css('slider');
elgg_load_css('toastr');
elgg_load_css('file-upload');
elgg_load_css('prettyPhoto');
elgg_load_css('flexslider');
elgg_load_css('main');

// Load Custom Js
elgg_load_js('jquery');
elgg_load_js('flexslider');
elgg_load_js('slidebars');
elgg_load_js('flowplayer');
elgg_load_js('bootstrap');
elgg_load_js('slider');
elgg_load_js('file-upload');
elgg_load_js('masonry');
elgg_load_js('mixitup');
elgg_load_js('switcher');
elgg_load_js('cookie');
elgg_load_js('prettyPhoto');
elgg_load_js('nicescroll');
elgg_load_js('toastr');
elgg_load_js('chosen');
elgg_load_js('user_posts');
elgg_load_js('charmbar');
elgg_load_js('timeline');
elgg_load_js('main');

// render content before head so that JavaScript and CSS can be loaded. See #4032
$messages = elgg_view('page/elements/messages', array('object' => $vars['sysmessages']));
$header = elgg_view('page/elements/header', $vars);
$navbar = elgg_view('page/elements/navbar', $vars);
$content = elgg_view('page/elements/body', $vars);
$footer = elgg_view('page/elements/footer', $vars);

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get Active Members
$active_members = find_active_users();
$circles_value = 'circles_' . $logged_user->guid;

// Get Profile Details
$data = 'data_' . $logged_user->guid;
$user_details = unserialize($logged_user->$data);
$user = (object) $user_details;
$body = '<div class="elgg-page elgg-page-default"><div class="elgg-page-messages">' . $messages . '</div>';
$body .= elgg_view('page/elements/topbar_wrapper', $vars);
ob_start();
