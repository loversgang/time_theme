<?php

function user_can_view($owner_circles, $post_circles, $post_owner_id, $logged_id = 0) {
    if (!$logged_id) {
        $logged_user = elgg_get_logged_in_user_entity();
        $logged_id = $logged_user->guid;
    }
    $can_view = FALSE;
    foreach ($post_circles as $circle_id) {
        if (array_key_exists($circle_id, $owner_circles) && is_array($owner_circles[$circle_id]) && in_array($logged_id, $owner_circles[$circle_id])) {
            $can_view = TRUE;
            break;
        }
    }
    if ($post_owner_id == $logged_id) {
        return TRUE;
    }
    return $can_view;
}

function time_stamp($session_time) {
    $time_difference = time() - $session_time;

    $seconds = $time_difference;
    $minutes = round($time_difference / 60);
    $hours = round($time_difference / 3600);
    $days = round($time_difference / 86400);
    $weeks = round($time_difference / 604800);
    $months = round($time_difference / 2419200);
    $years = round($time_difference / 29030400);
// Seconds
    if ($seconds <= 60) {
        echo "$seconds seconds ago";
    }
//Minutes
    else if ($minutes <= 60) {

        if ($minutes == 1) {
            echo "$minutes minute ago";
        } else {
            echo "$minutes minutes ago";
        }
    }
//Hours
    else if ($hours <= 24) {

        if ($hours == 1) {
            echo "$hours hour ago";
        } else {
            echo "$hours hours ago";
        }
    }
//Days
    else if ($days <= 7) {

        if ($days == 1) {
            echo "$days day ago";
        } else {
            echo "$days days ago";
        }
    }
//Weeks
    else if ($weeks <= 4) {

        if ($weeks == 1) {
            echo "$weeks week ago";
        } else {
            echo "$weeks weeks ago";
        }
    }
//Months
    else if ($months <= 12) {

        if ($months == 1) {
            echo "$months month ago";
        } else {
            echo "$months months ago";
        }
    }
//Years
    else {

        if ($years == 1) {
            echo "$years year ago";
        } else {
            echo "$years years ago";
        }
    }
}

function maybe_unserialize($original) {
    if (is_serialized($original))
        return @unserialize($original);
    return $original;
}

function maybe_serialize($data) {
    if (is_array($data) || is_object($data))
        return serialize($data);
    if (is_serialized($data, false))
        return serialize($data);
    return $data;
}

function is_serialized($data, $strict = true) {
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        if (false === $semicolon && false === $brace)
            return false;
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        case 'a' :
        case 'O' :
            return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}
