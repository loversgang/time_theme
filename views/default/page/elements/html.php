<?php
/**
 * Page shell for all HTML pages
 *
 * @uses $vars['head']        Parameters for the <head> element
 * @uses $vars['body_attrs']  Attributes of the <body> tag
 * @uses $vars['body']        The main content of the page
 */
// Set the content type
header("Content-type: text/html; charset=UTF-8");

$lang = get_current_language();

$attrs = "";
if (isset($vars['body_attrs'])) {
    $attrs = elgg_format_attributes($vars['body_attrs']);
    if ($attrs) {
        $attrs = " $attrs";
    }
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang; ?>" lang="<?php echo $lang; ?>">
    <head>
        <?php echo $vars["head"]; ?>
        <link href='<?php echo elgg_get_site_url(); ?>mod/time_theme/lib/css/colors/color_1.css' rel='stylesheet' id="colors-style" type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
            <script async src="//cdn.embedly.com/widgets/platform.js" charset="UTF-8"></script>
            <style>
                body{	
                    background-image: url('/ingynius/images/cover-1.jpg');
                }
                .main-pagination {
                    overflow: hidden;
                    text-align: center;
                    margin: 30px 0 30px 0;
                    padding-right: 10px;
                }

                .masonry_load_more {
                    padding: 5px 0;
                    cursor: pointer;
                    background: #000;
                    border-radius: 5px;
                }

                .masonry_load_more:hover {
                    background: #fff;
                    color: #010101;
                }
                .latest_img_slider {
                    width: 100% !important;
                    height: 200px !important;
                }
                .events_img {
                    width: 100% !important;
                    height: 50px !important;
                }
                .post_title_cl {
                    text-align: center;
                    font-weight: bold;
                }
                .post_result form.form.form-horizontal {
                    margin-top: -15px;
                }
                #post_area .col-md-2 {
                    width: 18.666667% !important;
                }
                #url {width: 100%;padding: 10px;border: 1px solid #F0F0F0;}
                #output{display:none;border: 1px solid #F0F0F0;overflow:hidden;padding:10px;}
                .image-extract{max-width:580px;text-align:center;}
                .btnNav{width:26px;height:26px;border:0;cursor:pointer;margin-top:10px}
                #prev-extract {background:url('previous.jpg');}
                #next-extract {background:url('next.jpg');}
                #prev-extract:disabled {opacity:0.5;}
                #next-extract:disabled {opacity:0.5;}
            </style>
    </head>
    <body<?php echo $attrs ?>>

        <?php
        if (!elgg_is_logged_in()) {
            //include('topbar_wrapper.php');
            echo $vars["body"];
        } else {
            echo $vars["body"];
        }
        ?>
    </body>
</html>