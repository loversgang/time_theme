<?php

$body .= ob_get_clean();
$head = elgg_view('page/elements/head', $vars['head']);
$params = array(
    'head' => $head,
    'body' => $body,
);
if (isset($vars['body_attrs'])) {
    $params['body_attrs'] = $vars['body_attrs'];
}
echo elgg_view("page/elements/html", $params);
