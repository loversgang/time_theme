<div class="col-md-2" style="width:22%">
    <?php include 'color_switcher.php'; ?>
    <div id="sidebar-wrapper" class="content_2 active">
        <div class="sidebar_closed" id="sidebar_closed" title="Close Sidebar" data-toggle="tooltip">
            <i class="icon-close"></i>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST IMAGES
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#images_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#images_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#images_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#images_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="images_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_newest_slider">
                                        <?php foreach ($files_latest as $file) { ?>
                                            <li data-thumb="<?php echo $file->getIconURL('medium'); ?>">
                                                <img class="img_options" src="<?php echo $file->getIconURL('master'); ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_featured_slider">
                                        <?php foreach ($files_featured as $file) { ?>
                                            <li data-thumb="<?php echo $file->getIconURL('medium'); ?>">
                                                <img class="img_options" src="<?php echo $file->getIconURL('master'); ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_popular_slider">
                                        <?php foreach ($files_popular as $file) { ?>
                                            <li data-thumb="<?php echo $file->getIconURL('medium'); ?>">
                                                <img class="img_options" src="<?php echo $file->getIconURL('master'); ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_all_slider">
                                        <?php foreach ($files as $file) { ?>
                                            <li data-thumb="<?php echo $file->getIconURL('medium'); ?>">
                                                <img class="img_options" src="<?php echo $file->getIconURL('master'); ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST VIDEOS
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#videos_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#videos_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#videos_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#videos_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="videos_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="/ingynius/images/E6KwXYmMiak-play.jpg">
                                            <img class="video_options" data-video="E6KwXYmMiak" src="/ingynius/images/E6KwXYmMiak-play.jpg"/>
                                        </li>
                                        <li data-thumb="/ingynius/images/OargwriB8ns-play.jpg">
                                            <img class="video_options" data-video="OargwriB8ns" src="/ingynius/images/OargwriB8ns-play.jpg"/>
                                        </li>
                                        <li data-thumb="/ingynius/images/XZ4X1wcZ1GE-play.jpg">
                                            <img class="video_options" data-video="XZ4X1wcZ1GE" src="/ingynius/images/XZ4X1wcZ1GE-play.jpg"/>
                                        </li>
                                        <li data-thumb="/ingynius/images/xaSH0d60Zso-play.jpg">
                                            <img class="video_options" data-video="xaSH0d60Zso" src="/ingynius/images/xaSH0d60Zso-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="/ingynius/images/XZ4X1wcZ1GE-play.jpg">
                                            <img class="video_options" data-video="XZ4X1wcZ1GE" src="/ingynius/images/XZ4X1wcZ1GE-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="/ingynius/images/xaSH0d60Zso-play.jpg">
                                            <img class="video_options" data-video="xaSH0d60Zso" src="/ingynius/images/xaSH0d60Zso-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="lightSlider1">
                                        <li data-thumb="/ingynius/images/E6KwXYmMiak-play.jpg">
                                            <img class="video_options" data-video="E6KwXYmMiak" src="/ingynius/images/E6KwXYmMiak-play.jpg"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;margin-top: 15px">
                <h5 class="post_title_cl colors-color">
                    UPCOMING EVENTS
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px">
                <div id="datetimepicker12" style="background-color: #fff"></div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">OCT</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">16</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">09:00 PM</div>
                    Architectire Conference
                </div>
                <div class="col-md-4">
                    <img src="/ingynius/images/cover-1.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">OCT</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">18</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">11:00 AM</div>
                    Test Event Conference
                </div>
                <div class="col-md-4">
                    <img src="/ingynius/images/cover-2.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                <div class="col-md-2">
                    <center>
                        <div style="color: orange;">NOV</div>
                        <div style="font-size: 30px;margin-top: -5px;color: #999999;">11</div>
                    </center>
                </div>
                <div class="col-md-6" style="color:#fff">
                    <div style="color: #999;">06:00 PM</div>
                    Diwali Festival
                </div>
                <div class="col-md-4">
                    <img src="/ingynius/images/cover-3.jpg" class="img img-responsive events_img"/>
                </div>
            </div>
            <div class="clearfix" style="margin-bottom: 50px"></div>
        </div>

        <!-- #sidebar -->

        <a id="go-top-button" class="test" href="#"><i class="fa fa-chevron-up"></i></a>
        <!--go-top-button-->

    </div>
</div>