<div class="col-md-2" style="width: 22%">
    <header id="header" class="header_1 content_3">
        <div class="logo <?php echo isset($pic_type) && $pic_type == 'square' ? 'logo-square' : 'logo-circle' ?>">
            <a href="#"><img src="<?php echo $logged_user->getIconURL('master'); ?>" alt="Logo"></a>
        </div> <!--/ .logo -->
        <h3 style="text-align: center" class="colors-color"><?php echo $logged_user->name ?></h3>
        <h4 class="tagline"><?php echo $logged_user->briefdescription ?></h4>
        <?php if ($show_quick_icons === TRUE) { ?>
            <div class="row profile-links">
                <div class="col-md-1"></div>
                <div class="col-md-2 profile-link">
                    <a href="" title="Message"><i class="fa fa-2x fa-envelope"></i></a>
                </div>
                <div class="col-md-2 profile-link">
                    <a href="" title="Follow"><i class="fa fa-2x fa-comments"></i></a>
                </div>
                <div class="col-md-2 profile-link">
                    <a href="" title="Add"><i class="fa fa-2x fa-plus-circle"></i></a>
                </div>
                <div class="col-md-2 profile-link">
                    <a href="" title="Video Chat"><i class="fa fa-2x fa-video-camera"></i></a>
                </div>
                <div class="col-md-2 profile-link">
                    <a href="" title="Video Chat"><i class="fa fa-2x fa-gift"></i></a>
                </div>
                <div class="col-md-1"></div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <h5 class="post_title_cl colors-color">
                    About Me
                </h5>
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="colors-color">Work:</div>
                    <div class="colors-color">Live:</div>
                    <div class="colors-color">Hometown:</div>
                    <div class="colors-color">Birthday:</div>
                </div>
                <div class="col-md-6" style="text-align: center;color:#fff;">
                    <div class="">XYZ Inc.</div>
                    <div class=""><?php echo $logged_user->location; ?></div>
                    <div class="">Foreign, EU</div>
                    <div class=""><?php echo date('M', mktime(0, 0, 0, $user->birth_month, 1, 2000)); ?> <?php echo $user->birth_date; ?>th</div>
                </div>
                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                    <div class="colors-color-hover">
                        <a href="interests.php">
                            <i class="fa fa-user"></i><br/>
                            Interests
                        </a>
                    </div>
                </div>
                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                    <div class="colors-color-hover">
                        <a href="resume.php">
                            <i class="fa fa-list"></i><br/>
                            Resume
                        </a>
                    </div>
                </div>
                <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                    <div class="colors-color-hover">
                        <a href="contact.php">
                            <i class="fa fa-paper-plane"></i><br/>
                            Contact
                        </a>
                    </div>
                </div>
                <div class="col-md-12" style="text-align: center;color:#fff;margin-top: 10px;">
                    <button class="btn btn-default btn-xs">More</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <h5 class="post_title_cl colors-color">
                    ONLINE
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px">
                <!-- Nav tabs -->
                <div class="card" style="box-shadow: none">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#online_newest" aria-controls="home" role="tab" data-toggle="tab">Newest</a></li>
                        <li role="presentation"><a href="#online_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#online_active" aria-controls="messages" role="tab" data-toggle="tab">Active</a></li>
                        <li role="presentation"><a href="#online_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="online_newest">
                            <div class="row-fluid">
                                <?php foreach ($online_members as $member) { ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <img style="width: 50px;height: 40px" src="<?php echo $member->getIconURL('medium') ?>" class="img img-responsive img-circle"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_featured">
                            <div class="row-fluid">
                                <?php foreach ($members as $member) { ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <img style="width: 50px;height: 40px" src="<?php echo $member->getIconURL('medium') ?>" class="img img-responsive img-circle"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_active">
                            <div class="row-fluid">
                                <?php foreach ($members as $member) { ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <img style="width: 50px;height: 40px" src="<?php echo $member->getIconURL('medium') ?>" class="img img-responsive img-circle"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_all">
                            <div class="row-fluid">
                                <?php foreach ($members as $member) { ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <img style="width: 50px;height: 40px" src="<?php echo $member->getIconURL('medium') ?>" class="img img-responsive img-circle"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        GROUPS
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#groups_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#groups_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#groups_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#groups_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="groups_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_latest_slider">
                                        <?php foreach ($groups_latest as $group) { ?>
                                            <li data-thumb="<?php echo $group->getIconURL('medium') ?>">
                                                <img class="group_img" src="<?php echo $group->getIconURL('master') ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_featured_slider">
                                        <?php foreach ($groups_featured as $group) { ?>
                                            <li data-thumb="<?php echo $group->getIconURL('medium') ?>">
                                                <img class="group_img" src="<?php echo $group->getIconURL('master') ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_popular_slider">
                                        <?php foreach ($groups_popular as $group) { ?>
                                            <li data-thumb="<?php echo $group->getIconURL('medium') ?>">
                                                <img class="group_img" src="<?php echo $group->getIconURL('large') ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_all_slider">
                                        <?php foreach ($groups as $group) { ?>
                                            <li data-thumb="<?php echo $group->getIconURL('medium') ?>">
                                                <img class="group_img" src="<?php echo $group->getIconURL('large') ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        PAGES
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#pages_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#pages_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#pages_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#pages_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="pages_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="temp2_lightSlider1">
                                        <li data-thumb="/ingynius/images/cover-1.jpg">
                                            <img src="/ingynius/images/cover-1.jpg" />
                                        </li>
                                        <li data-thumb="/ingynius/images/cover-2.jpg">
                                            <img src="/ingynius/images/cover-2.jpg" />
                                        </li>
                                        <li data-thumb="/ingynius/images/cover-3.jpg">
                                            <img src="/ingynius/images/cover-3.jpg" />
                                        </li>
                                        <li data-thumb="/ingynius/images/cover-4.jpg">
                                            <img src="/ingynius/images/cover-4.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="temp2_lightSlider">
                                        <li data-thumb="/ingynius/images/cover-3.jpg">
                                            <img src="/ingynius/images/cover-3.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="temp2_lightSlider">
                                        <li data-thumb="/ingynius/images/cover-1.jpg">
                                            <img src="/ingynius/images/cover-1.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="temp2_lightSlider">
                                        <li data-thumb="/ingynius/images/cover-4.jpg">
                                            <img src="/ingynius/images/cover-4.jpg" />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="copyright">
            <p style="margin-bottom:50px;">Copyright &copy; <?php echo date('Y') . ' - ' . date('Y', strtotime(date('Y') . '+ 1 Year')) ?>.</p>		
        </div>
    </header>
</div>