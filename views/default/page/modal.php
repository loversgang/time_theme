<?php
// Get Circles/Cliques
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:10000000">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sample Post Title</h4>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class="col-md-8">
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-12">
                                <div id="content_data"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <span style="font-size: 15px"><i style="color:darkred" class="fa fa-user"></i> John Doe</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-clock-o"></i> Posted on 2015-10-02</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-map-marker"></i> Sydney, Australia</span>
                                <span style="color:#999;margin-left: 10px"><i style="color:darkred" class="fa fa-arrows-alt"></i> <a id="post_link_attr" style="color:#3b5998" href="">View More</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12" style="max-height: 350px; overflow: auto;background-color: #eee;padding-top: 10px">
                            <?php for ($i = 0; $i < 6; $i++) { ?>
                                <div class="row" style="margin-bottom: 10px;padding-bottom: 5px;border-bottom: 1px #fff solid;">
                                    <div class="col-md-4">
                                        <img class="img img-circle" src="/ingynius/images/cover-1.jpg" alt="Logo">
                                    </div>
                                    <div class="col-md-8">
                                        <div style="margin-left: -20px;">
                                            <span style="color:grey">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry.</span>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img img-circle" src="/ingynius/images/cover-2.jpg" alt="Logo">
                                </div>
                                <div class="col-md-8">
                                    <div style="margin-left: -20px;">
                                        <div class="form-group">
                                            <input placeholder="Post Comment..." type="text" class="form-control"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border:none"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div id="post_content"></div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="circles_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Add To Circles</b></h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div class="row-fluid">
                    <div class="col-md-12">
                        <form>
                            <?php
                            if ($logged_user->$circles_value) {
                                $user_circles = maybe_unserialize($logged_user->$circles_value);
                            }
                            ?>
                            <?php foreach ($circles as $circle) { ?>
                                <div>
                                    <input id="box<?php echo $circle->guid; ?>" type="checkbox" value="<?php echo $circle->guid; ?>" <?php echo in_array($user->guid, $user_circles[$circle->guid]) ? 'checked' : '' ?>>
                                    <label for="box<?php echo $circle->guid; ?>">
                                        <span><?php echo $circle->title; ?></span>
                                    </label>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" id="user_add_circles" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</div>