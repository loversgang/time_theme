<?php
include 'functions.php';
include 'groups.php';
include 'members.php';
include 'files.php';
include 'pages.php';
include 'header.php';
?>
<div class="col-md-12" style="top:70px;">
    <?php if (elgg_is_logged_in()) { ?>
        <?php include 'top.php'; ?>
        <?php include 'charmbar.php'; ?>
        <?php include 'content.php'; ?>
        <?php
        if ((elgg_get_context() == 'activity2')) {
            echo elgg_view('page/content');
        }
        ?>
        <?php include 'bottom.php'; ?>
        <?php include 'modal.php'; ?>
    <?php } else { ?>
        <div class="col-md-4">
            <?php
            echo $vars['area1'];
            echo elgg_view('/ingynius/register');
            ?>
        </div>
        <div class="col-md-4">
            <?php echo $content; ?>
        </div>
        <div class="col-md-4">
            <?php include 'color_switcher.php'; ?>
        </div>
    <?php } ?>
</div>
<?php include 'footer.php'; ?>