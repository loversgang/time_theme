<?php
$entities = elgg_get_entities(array(
    'limit' => FALSE,
        ));
?>
<div class="col-md-2"></div>
<div class="col-md-8 post_box">
    <div class="row">
        <div>
            <div style="text-align: center;">
                <div class="col-md-2" id="post_text">
                    <i class="fa fa-font fa-2x colors-color-hover" style="color:#444"></i><br>
                    <span>Text</span>
                </div>
                <div class="col-md-2" id="post_photo">
                    <i class="fa fa-camera fa-2x colors-color-hover" style="color:#d95e40"></i><br>
                    <span>Photo</span>
                </div>
                <div class="col-md-2" id="post_link">
                    <i class="fa fa-link fa-2x colors-color-hover" style="color:#56bc8a"></i><br>
                    <span>Link</span>
                </div>
                <div class="col-md-2" id="post_audio">
                    <i class="fa fa-volume-up fa-2x colors-color-hover" style="color:#529ecc"></i><br>
                    <span>Audio</span>
                </div>
                <div class="col-md-2" id="post_video">
                    <i class="fa fa-film fa-2x colors-color-hover" style="color:#748089"></i><br>
                    <span>Video</span>
                </div>
                <div class="col-md-2" id="post_event">
                    <i class="fa fa-calendar fa-2x colors-color-hover" style="color:#f2992e"></i><br>
                    <span>Event</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2"></div>
<div class="clearfix"></div>
<div class="col-md-12" style="padding: 0">
    <div class="bhoechie-tab" style="min-height: 856px">
        <div class="bhoechie-tab-content active">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <?php foreach ($entities as $entity) { ?>
                    <b>
                        <?php
                        $subtype = get_subtype_from_id($entity->subtype);
                        ?>
                    </b>
                    <?php
                    if ($entity->type == 'group' || $subtype == 'file' || $subtype == 'text_content') {
                        $user_entity = elgg_get_entities(array(
                            'type' => 'user',
                            'guid' => $entity->owner_guid,
                        ));
                        $post_owner = $user_entity[0];
                        $user_circles_list = array();
                        $user_circles_value = 'circles_' . $post_owner->guid;
                        if ($post_owner->$user_circles_value) {
                            $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
                        }
                        $owner = get_user_entity_as_row($entity->owner_guid);
                        if ($entity->type == 'group') {
                            $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
                            ?>
                            <div class="item" data-type="media">
                                <div class="row item_content">
                                    <div class="col-md-2" style="width:65px">
                                        <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                    </div>
                                    <div class="col-md-8" style="margin-left: -20px;">
                                        <i>
                                            <b>
                                                <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                    <?php echo $owner->name; ?>
                                                </a>
                                            </b>
                                            <font style="color:#999">
                                            <?php echo $line ?>
                                            <br/>
                                            <?php echo time_stamp($entity->time_created); ?>
                                            </font>
                                        </i> . <?php $logged_user->guid == $entity->owner_guid ? '<i style="color:#000" class="fa fa-trash-o"></i>' : '' ?>
                                    </div>
                                    <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                        <div class="img-list">
                                            <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                            <span class="text-content">
                                                <span>
                                                    <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                    <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                    <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                    <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                    <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                                </span>
                                            </span>
                                        </div>
                                        <br/>
                                        <div class="post_content" style="padding:5px">
                                            <?php echo substr($entity->description, 0, 200); ?>..
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } elseif ($subtype == 'file') {
                            $line = 'added new photo <a href="' . elgg_get_site_url() . 'file/view/' . $entity->guid . '/' . str_replace(' ', '-', $entity->title) . '">' . $entity->title . '</a>';
                            ?>
                            <div class="item" data-type="media">
                                <div class="row item_content">
                                    <div class="col-md-2" style="width:65px">
                                        <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                    </div>
                                    <div class="col-md-8" style="margin-left: -20px;">
                                        <i>
                                            <b>
                                                <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                    <?php echo $owner->name; ?>
                                                </a>
                                            </b>
                                            <font style="color:#999">
                                            <?php echo $line ?>
                                            <br/>
                                            <?php echo time_stamp($entity->time_created); ?>
                                            </font>
                                        </i>
                                    </div>
                                    <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                        <div class="img-list">
                                            <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                            <span class="text-content">
                                                <span>
                                                    <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                    <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                    <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                    <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                    <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                                </span>
                                            </span>
                                        </div>
                                        <br/>
                                        <div class="post_content" style="padding:5px">
                                            <?php echo substr($entity->description, 0, 200); ?>..
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } elseif ($subtype == 'text_content') {
                            $post = maybe_unserialize($entity->description);
                            $collection = get_access_collection($entity->access_id);
                            $post_location = $collection->name;
                            $line = 'updated status';
                            if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid))
                                continue;
                            ?>
                            <div class="item" data-type="text" style="position: absolute; left: 0px; top: 0px;">
                                <div class="row item_content">
                                    <div class="col-md-2" style="width:65px">
                                        <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                    </div>
                                    <div class="col-md-8" style="margin-left: -20px;">
                                        <i>
                                            <b>
                                                <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                    <?php echo $owner->name; ?>
                                                </a>
                                            </b>
                                            <font style="color:#999">
                                            <?php echo $line ?>
                                            <br/>
                                            <?php echo time_stamp($entity->time_created); ?>
                                            </font>
                                        </i>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="post_content">
                                            <?php echo substr($post['text_content'], 0, 200); ?>..
                                            <br/>
                                            <br/>
                                            <span class="location">
                                                <?php echo $post['text_location'] ? $post['text_location'] . ' . ' : ''; ?><?php echo $post_location; ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else {
                            $line = 'Text';
                        }
                    } else {
                        continue;
                    }
                }
                ?>
            </div>
            <div class="col-md-12">
                <div class="main-pagination">
                    <div class="masonry_load_more colors-color">
                        <h3 id="load_more_text">Load More</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="only_mine_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="all_groups_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div class="col-md-12" style="margin-bottom: 10px;">                
                <div class="col-md-1"></div>
                <div class="col-md-10" style="background-color:#000;padding: 0px">
                    <div class="col-md-12" style="padding:0px">
                        <div class="row item_content" style="background-color: #000;">
                            <div class="col-md-5" style="padding:0px">
                                <img class="img_options"  src="<?php echo $logged_user->getIconURL('large'); ?>" style="margin-top: -5px;height: 200px;" />
                                <h2 class="post_title_cl" style="color:#9CD759;margin-top: 30px;">
                                    <i class="fa fa-circle"></i> Online
                                </h2>
                            </div>
                            <div class="col-md-7" style="padding:5px">
                                <div class="post_user_detail">
                                    <h4 style="text-align: center" class="colors-color">
                                        <?php echo $logged_user->name; ?>
                                    </h4>
                                    <h4 style="text-align: center" class="colors-color">@<?php echo $logged_user->username; ?></h4>
                                    <p style="color:#fff;text-align: center">
                                        <i><?php echo $logged_user->briefdescription ?></i>
                                    </p>

                                    <div class="col-md-12">
                                        <h5 class="post_title_cl colors-color">
                                            About Me
                                        </h5>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                                        <div class="col-md-6" style="text-align: center;">
                                            <div class="colors-color">Work:</div>
                                            <div class="colors-color">Live:</div>
                                            <div class="colors-color">Hometown:</div>
                                            <div class="colors-color">Birthday:</div>
                                        </div>
                                        <div class="col-md-6" style="text-align: center;color:#fff;">
                                            <div class="">XYZ Inc.</div>
                                            <div class=""><?php echo $logged_user->location; ?></div>
                                            <div class="">Foreign, EU</div>
                                            <div class=""><?php echo date('M', mktime(0, 0, 0, $user->birth_month, 1, 2000)); ?> <?php echo $user->birth_date; ?>th</div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="interests.php">
                                                    <i class="fa fa-user"></i><br/>
                                                    Interests
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="resume.php">
                                                    <i class="fa fa-list"></i><br/>
                                                    Resume
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="contact.php">
                                                    <i class="fa fa-paper-plane"></i><br/>
                                                    Contact
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <button class="btn btn-default btn-xs">More</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12 colors-color" style="padding: 0px;margin-left: -8px;">
                                        <div class="col-md-2">
                                            <i class="fa fa-inbox fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-cog  fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-plus  fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-eye fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-video-camera fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-gift fa fa-2x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="clearfix"></div>
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="show_online_members"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
    </div>
</div>