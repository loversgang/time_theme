<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get All Files
$files = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'file',
        ));

// Get Latest Files
$files_latest = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'file',
    'limit' => 10
        ));

// Get Featured Files
$files_featured = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'file',
    'limit' => 2
        ));

// Get Popular Files
$files_popular = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'file',
    'limit' => 3
        ));
