<?php $user = elgg_get_page_owner_entity(); ?>
<div class="row-fluid" style="background: #fff">
    <div class="col-md-12" style="margin-top: 1px;padding: 0px">
        <div class="fb-profile">
            <img align="left" class="fb-image-lg" src="<?php echo $user->getIconURL('large'); ?>" alt="Profile image example">
            <span id="page_photo"><img class="fb-image-profile thumbnail" align="left" src="<?php echo $user->getIconURL('large'); ?>"></span>
            <div class="fb-profile-text" style="background-color: #fff;">
                <?php if ($logged_user->guid != $user->guid) { ?>
                    <div class="add_to_circles_button">
                        <button class="btn btn-default" id="add_to_circles" data-user_id="<?php echo $user->guid; ?>"><i class="fa fa-plus-circle"></i> Add</button>
                    </div>
                <?php } ?>
                <h1><span class="page_name"><?php echo $user->name; ?></span></h1>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding: 0px">
        <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
            <?php
            $entities = elgg_get_entities(array(
                'limit' => FALSE,
                'owner_guid' => $user->guid,
            ));
            ?>
            <?php foreach ($entities as $entity) { ?>
                <b>
                    <?php
                    $subtype = get_subtype_from_id($entity->subtype);
                    ?>
                </b>
                <?php
                if ($entity->type == 'group' || $subtype == 'file' || $subtype == 'text_content') {
                    $user_entity = elgg_get_entities(array(
                        'type' => 'user',
                        'guid' => $entity->owner_guid,
                    ));
                    $post_owner = $user_entity[0];
                    $owner = get_user_entity_as_row($entity->owner_guid);
                    if ($entity->type == 'group') {
                        $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
                        ?>
                        <div class="item" data-type="media">
                            <div class="row item_content">
                                <div class="col-md-2" style="width:65px">
                                    <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i>
                                        <b>
                                            <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                <?php echo $owner->name; ?>
                                            </a>
                                        </b>
                                        <font style="color:#999">
                                        <?php echo $line ?>
                                        <br/>
                                        <?php echo time_stamp($entity->time_created); ?>
                                        </font>
                                    </i>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                    <div class="img-list">
                                        <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                        <span class="text-content">
                                            <span>
                                                <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                            </span>
                                        </span>
                                    </div>
                                    <br/>
                                    <div class="post_content" style="padding:5px">
                                        <?php echo substr($entity->description, 0, 200); ?>..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } elseif ($subtype == 'file') {
                        $line = 'added new photo <a href="' . elgg_get_site_url() . 'file/view/' . $entity->guid . '/' . str_replace(' ', '-', $entity->title) . '">' . $entity->title . '</a>';
                        ?>
                        <div class="item" data-type="media">
                            <div class="row item_content">
                                <div class="col-md-2" style="width:65px">
                                    <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i>
                                        <b>
                                            <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                <?php echo $owner->name; ?>
                                            </a>
                                        </b>
                                        <font style="color:#999">
                                        <?php echo $line ?>
                                        <br/>
                                        <?php echo time_stamp($entity->time_created); ?>
                                        </font>
                                    </i>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                    <div class="img-list">
                                        <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                        <span class="text-content">
                                            <span>
                                                <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                            </span>
                                        </span>
                                    </div>
                                    <br/>
                                    <div class="post_content" style="padding:5px">
                                        <?php echo substr($entity->description, 0, 200); ?>..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } elseif ($subtype == 'text_content') {
                        $post = maybe_unserialize($entity->description);
                        $collection = get_access_collection($post['content_privacy']);
                        $post_location = $collection->name;
                        $line = 'updated status';
                        ?>
                        <div class="item" data-type="text" style="position: absolute; left: 0px; top: 0px;">
                            <div class="row item_content">
                                <div class="col-md-2" style="width:65px">
                                    <img src="<?php echo $post_owner->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i>
                                        <b>
                                            <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                <?php echo $owner->name; ?>
                                            </a>
                                        </b>
                                        <font style="color:#999">
                                        <?php echo $line ?>
                                        <br/>
                                        <?php echo time_stamp($entity->time_created); ?>
                                        </font>
                                    </i>
                                </div>
                                <div class="col-md-12">
                                    <div class="post_content">
                                        <?php echo substr($post['text_content'], 0, 200); ?>..
                                        <br/>
                                        <br/>
                                        <span class="location">
                                            <?php echo $post['text_location']; ?> . <?php echo $post_location; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    } else {
                        $line = 'Text';
                    }
                } else {
                    continue;
                }
            }
            ?>
        </div>
    </div>
</div>