<?php
require_once(dirname(__FILE__) . "/engine/start.php");

$router = _elgg_services()->router;
$request = _elgg_services()->request;

if (!$router->route($request)) {
    forward('', '404');
}
if (isset($_GET['action']) && $_GET['action']) {
    extract($_GET);
    if ($action === 'more_home_posts') {
        $entities = elgg_get_entities(array(
            'limit' => FALSE,
        ));
        ?>
        <div class="col-md-12" style="padding: 0">
            <div class="bhoechie-tab" style="min-height: 856px">
                <div class="bhoechie-tab-content active">
                    <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                        <div class="item" data-type="media">
                            <div class="row item_content">
                                <div class="col-md-2" style="width:65px">
                                    <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                </div>
                                <div class="col-md-8" style="margin-left: -20px;">
                                    <i>
                                        <b>
                                            <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                <?php echo $owner->name; ?>
                                            </a>
                                        </b>
                                        <font style="color:#999">
                                        <?php echo $line ?>
                                        <br/>
                                        <?php echo time_stamp($entity->time_created); ?>
                                        </font>
                                    </i>
                                </div>
                                <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                    <div class="img-list">
                                        <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                        <span class="text-content">
                                            <span>
                                                <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                            </span>
                                        </span>
                                    </div>
                                    <br/>
                                    <div class="post_content" style="padding:5px">
                                        <?php echo substr($entity->description, 0, 200); ?>..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php foreach ($entities as $entity) { ?>
                            <b>
                                <?php
                                $subtype = get_subtype_from_id($entity->subtype);
                                ?>
                            </b>
                            <?php
                            if ($entity->type == 'group' || $subtype == 'file') {
                                $owner = get_user_entity_as_row($entity->owner_guid);
                                if ($entity->type == 'group') {
                                    $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
                                } elseif ($subtype == 'file') {
                                    $line = 'added new photo <a href="' . elgg_get_site_url() . 'file/view/' . $entity->guid . '/' . str_replace(' ', '-', $entity->title) . '">' . $entity->title . '</a>';
                                } else {
                                    $line = '';
                                }
                                ?>
                                <div class="item" data-type="media">
                                    <div class="row item_content">
                                        <div class="col-md-2" style="width:65px">
                                            <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                                        </div>
                                        <div class="col-md-8" style="margin-left: -20px;">
                                            <i>
                                                <b>
                                                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                                        <?php echo $owner->name; ?>
                                                    </a>
                                                </b>
                                                <font style="color:#999">
                                                <?php echo $line ?>
                                                <br/>
                                                <?php echo time_stamp($entity->time_created); ?>
                                                </font>
                                            </i>
                                        </div>
                                        <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                            <div class="img-list">
                                                <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                                <span class="text-content">
                                                    <span>
                                                        <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                                        <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                                        <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                                        <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                                        <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                                    </span>
                                                </span>
                                            </div>
                                            <br/>
                                            <div class="post_content" style="padding:5px">
                                                <?php echo substr($entity->description, 0, 200); ?>..
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } else {
                                continue;
                            }
                            ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}
if (isset($_POST['action']) && $_POST['action']) {
    extract($_POST);
    if ($action == 'get_post_text_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                                <option>Private</option>
                                <option>Protected</option>
                                <option>Other</option>
                            </select>
                            <div class="input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_photo_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12">
                    <center>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </center>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Add Photo To Collection</label>
                        <div class="input-group">
                            <input placeholder="Enter Title" type="text" class="form-control border-radius" />
                            <div class="input-group-addon border-radius black-green-button colors-color">Create Collection</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_link_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Grab Link Info</label>
                        <div class="input-group">
                            <input id="url" placeholder="Enter URL" type="text" class="form-control border-radius" />
                            <div id="get_url_info" class="input-group-addon border-radius black-green-button colors-color">Fetch</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="output"></div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius  black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_audio_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input type="text" class="form-control border-radius" id="audioUrl" name="audioUrl" placeholder="Eg. 101276036" value="101276036"/>
                        <div id="embed_soundcloud_audio" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="soundcloud_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">

                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>                         
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">Post</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Audio To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_video_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input value="https://www.youtube.com/embed/E6KwXYmMiak" type="text" class="form-control border-radius" id="videoUrl" name="videoUrl" placeholder="Youtube Video URL"/>
                        <div id="embed_youtube_video" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="youtube_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Video To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }

    if ($action == 'get_post_event_html') {
        ?>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <input placeholder="Enter Event Title" type="text" class="form-control border-radius" />
            </div>
            <div class="form-group">
                <label class="control-label">Enter Event Date</label>
                <input type="date" class="form-control border-radius" />
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <div class="input-group">
                    <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                    <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="location_input" style="display: none">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                        <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                    </div>
                </div>
                <div id="location_html">
                    <center><img src="images/splash-screen.png"/></center>
                </div><br />
            </div>
            <div class="form-group">
                <textarea placeholder="Enter Event Description" type="text" class="form-control border-radius" /></textarea>
            </div>
            <label name="select_collection">Add Event To Collection</label>
            <div class="form-group">
                <select class="form-control border-radius">
                    <option>Collection Name</option>
                </select>
            </div>
        </div>

    <?php } ?>
    <?php if ($action == 'get_post_collection_html') { ?>

        <?php
    }
    if ($action == 'save_text_status') {
        $obj = new ElggObject();
        ?>

        <?php
    }
}
?>