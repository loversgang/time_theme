$(document).ready(function () {
    $(document).on('click', '#add_to_circles', function () {
        var circles_modal = $('#circles_modal');
        circles_modal.modal();
    });
    $(document).on('click', '#user_add_circles', function () {
        toastr.remove();
        var user_id = $('#add_to_circles').data('user_id');
        var user_circles_array = [];
        $('#circles_modal.modal input[type=checkbox]:checked').each(function () {
            var circle = $(this).val();
            user_circles_array.push(circle);
        });
        if (user_circles_array.length) {
            $.post(ajax_url, {action: 'save_user_circles', user_id: user_id, user_circles_array: user_circles_array}, function (data) {
//                console.log(data); return;
                if (data) {
                    window.location.reload();
                } else {
                    toastr.error('Something Went Wrong!', 'Error');
                }
            });
        } else {
            toastr.error('Please Select Circle!', 'Error');
        }
    });
});