jQuery(document).ready(function ($) {

    "use strict";
    window.base_url = $(document).find('#base_url').attr('href');
    window.ajax_url = base_url + 'mod/time_theme/ajax.php';
    /* ---------------------------------------------------------------------- */
    /*	------------------------------- Loading ----------------------------- */
    /* ---------------------------------------------------------------------- */

    /*Page Preloading*/
    $(window).load(function () {
        $('.preloader').fadeOut();
        $('.wrapper-content').css('opacity', '1').fadeIn();
        $('#custumize-style').fadeIn();
    });

    /* ---------------------------------------------------------------------- */
    /* --------------------------- Scroll body ------------------------------ */
    /* ---------------------------------------------------------------------- */

    /* niceScroll */
    $("body").niceScroll({
        touchbehavior: false,
        scrollspeed: 60,
        mousescrollstep: 38,
        cursorwidth: 10,
        cursorborder: 0,
        autohidemode: false,
        zindex: 99999999,
        horizrailenabled: false,
        cursorborderradius: 20,
        cursorcolor: "#333"
    });

    $("#hedaer_mobile").niceScroll({
        touchbehavior: true,
        horizrailenabled: false,
        cursorwidth: 4,
        scrollspeed: 80,
        cursorborderradius: 20,
        mousescrollstep: 60,
        cursorborder: 0,
        cursorcolor: "#333"
    });

    /* ---------------------------------------------------------------------- */
    /* --------------------------- Scroll tabs ------------------------------ */
    /* ---------------------------------------------------------------------- */
    /* niceScroll */
    $("#sidebar-wrapper").niceScroll({
        touchbehavior: false,
        horizrailenabled: false,
        cursorwidth: 10,
        scrollspeed: 80,
        cursorborderradius: 20,
        mousescrollstep: 60,
        cursorborder: 0,
//        zindex: 99999999,
        autohidemode: false,
        cursorcolor: "#5f5f5f"
    });

    $("#header").niceScroll({
        touchbehavior: false,
        scrollspeed: 60,
        mousescrollstep: 40,
        cursorwidth: 10,
        cursorborder: 0,
        autohidemode: false,
        zindex: 99999999,
        horizrailenabled: false,
        cursorborderradius: 15,
        cursorcolor: "#5f5f5f"
    });

    /* niceScroll form contact */
    $("#contentContact").niceScroll({
        cursorcolor: '#757a7a',
        cursorborder: '0px solid #fff',
        cursoropacitymax: '0.5',
        cursorborderradius: '0px',
        zindex: 99999999,
        cursorwidth: 7
    });

    /* ---------------------------------------------------------------------- */
    /* ----------------------- scroll to top button ------------------------- */
    /* ---------------------------------------------------------------------- */

    var shown = false;
    //scroll event
    $(window).scroll(function () {

        //show/hide scroll to top button
        if ($(this).scrollTop() > 500) {
            if (!shown) {
                $('#go-top-button').stop().addClass("visible");
                shown = true;
            }
        } else {
            if (shown) {
                $('#go-top-button').stop().removeClass("visible");
                shown = false;
            }
        }

    });

    //animate scroll to top
    $('#go-top-button').click(function (event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 600);
    });


    /* ---------------------------------------------------------------------- */
    /* --------------------- opens the sidebar menu -------------------------- */
    /* ---------------------------------------------------------------------- */

    $("#sidebar-toggle").click(function (e) {

        e.preventDefault();

        $("#sidebar-wrapper").toggleClass("active");

        if ($("#sidebar-wrapper").hasClass('active')) {
            $("#sidebar-wrapper h2.close_sidebar").css("display", "block");
            $("#sidebar-wrapper .widget").addClass("active");
        } else {
            $("#sidebar-wrapper h2.close_sidebar").css("display", "none");
            $("#sidebar-wrapper .widget").removeClass("active");
        }
    });

    $("#btn_sidebar_wrapper").click(function (e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
        $('#nav_icon_sidebar').removeClass("fa-chevron-left fa-chevron-right");
        if ($("#sidebar-wrapper").hasClass('active')) {
            $("#btn_sidebar_wrapper").addClass("active");
            $('#nav_icon_sidebar').addClass("fa-chevron-right");
        } else {
            $("#btn_sidebar_wrapper").removeClass("active");
            $('#nav_icon_sidebar').addClass("fa-chevron-left");
            setTimeout(function () {
                $('#go-top-button2').click();
                if ($('.user_options .options_pane').hasClass('active')) {
                    $('.user_options .options_pane').removeClass('active');
                }
            }, 200);
        }
    });


    /* ---------------------------------------------------------------------- */
    /* ------------------- opens the menu responsive ------------------------ */
    /* ---------------------------------------------------------------------- */

    $("#btn_open_menu").click(function (e) {

        e.preventDefault();

        $("#hedaer_mobile").toggleClass("active");

        if ($("#hedaer_mobile").hasClass('active')) {
            $(".wrapper-content").addClass("active");
            $(this).find('i').removeClass('fa fa-bars');
            $(this).find('i').addClass('icon-close');
            $("#hedaer_mobile .menu_closed").css('display', 'block');
        } else {
            $(".wrapper-content").removeClass("active");
            $(this).find('i').addClass('fa fa-bars');
            $(this).find('i').removeClass('icon-close');
            $("#hedaer_mobile .menu_closed").css('display', 'none');
        }

        return false;
    });


    //closed menu in responsive
    $("#menu_closed").click(function (e) {

        e.preventDefault();

        $("#hedaer_mobile").removeClass('active')
        $(".wrapper-content").removeClass("active");
        $("#btn_open_menu").find('i').addClass('fa fa-bars');
        $("#btn_open_menu").find('i').removeClass('icon-close');
        $("#hedaer_mobile .menu_closed").css('display', 'none');

        return false;
    });

    //open sidebar in responsive
    $("#btn_open_sidebar").click(function (e) {

        e.preventDefault();


        if ($(this).hasClass('sidebar_close')) {

            $(this).removeClass('sidebar_close');
            $(this).find('i').removeClass('icon-action-undo');
            $(this).find('i').addClass('icon-action-redo');
            $("#sidebar-wrapper").addClass("sidebar_active");
            $(".wrapper-content").addClass("sidebar_active");

        } else {

            $(this).addClass('sidebar_close');
            $(this).find('i').addClass('icon-action-undo');
            $(this).find('i').removeClass('icon-action-redo');
            $("#sidebar-wrapper").removeClass("sidebar_active");
            $(".wrapper-content").removeClass("sidebar_active");

        }

        return false;

    });


    $("#sidebar_closed").click(function (e) {

        e.preventDefault();

        $("#btn_open_sidebar").addClass('sidebar_close');
        $("#btn_open_sidebar").find('i').addClass('icon-action-undo');
        $("#btn_open_sidebar").find('i').removeClass('icon-action-redo');
        $("#sidebar-wrapper").removeClass("sidebar_active");
        $(".wrapper-content").removeClass("sidebar_active");

        return false;

    });


    // On lie l'événement resize à la fonction
    function resize_windows() {

        if (window.matchMedia("(min-width: 1025px)").matches) {

            $(".wrapper-content").removeClass("active");
            $("#header").removeClass("active");
            $("#hedaer_mobile").removeClass("active");
            $("#sidebar-wrapper").removeClass("sidebar_active");
            $(".wrapper-content").removeClass("sidebar_active");
            $('.wrapper-content .header-main').css('display', 'none');
            $("#btn_open_sidebar").addClass('sidebar_close');
            $("#btn_open_sidebar").find('i').removeClass('icon-action-undo');
            $("#btn_open_sidebar").find('i').addClass('icon-action-redo');
            $("#btn_open_menu").find('i').removeClass('fa fa-bars');
            $("#btn_open_menu").find('i').addClass('icon-close');
            $("#hedaer_mobile .menu_closed").css('display', 'none');

        } else {
            $('#sidebar-wrapper').removeClass("active");
            $('#btn_sidebar_wrapper').removeClass("active");
            $('.nav_icon_sidebar').removeClass("active");
            $('.wrapper-content .header-main').css('display', 'block');
            $("#btn_open_sidebar").find('i').addClass('icon-action-undo');
            $("#btn_open_sidebar").find('i').removeClass('icon-action-redo');
            $("#btn_open_menu").find('i').addClass('fa fa-bars');
            $("#btn_open_menu").find('i').removeClass('icon-close');
            $("#hedaer_mobile .menu_closed").css('display', 'block');

        }

    }

    window.addEventListener('load', resize_windows, false);
    window.addEventListener('resize', resize_windows, false);



    /* ---------------------------------------------------------------------- */
    /* -------------------- Closes the sidebar menu ------------------------- */
    /* ---------------------------------------------------------------------- */

    $("#sidebar-close").click(function (e) {
        e.preventDefault();

        $("#sidebar-wrapper").toggleClass("active");
        $("#sidebar-wrapper h2.close_sidebar").css("display", "none");
    });


    /* ---------------------------------------------------------------------- */
    /* ----------------------- tooltip icon social -------------------------- */
    /* ---------------------------------------------------------------------- */

    $('.tooltip-social li a').tooltip({
        placement: "top"
    });

    $('#tooltip_social li a').tooltip({
        placement: "top"
    });

    //tooltip share post
    $('#tooltip-share li a').tooltip({
        placement: "top"
    });

    //tooltip flicker
    $('#tooltip-flicker li').tooltip({
        placement: "top"
    });

    //tooltip social about
    $('#social_ul li a').tooltip({
        placement: "top"
    });

    //tooltip social header
    $('#header_social_ul li a').tooltip({
        placement: "top"
    });

    //tooltip social header
    $('#header_social_ul_about li a').tooltip({
        placement: "top"
    });

    //tooltip social contact
    $('#contact_social_ul li a').tooltip({
        placement: "top"
    });

    //tooltip button toggle sidebar
    $('#sidebar-toggle').tooltip({
        placement: "right"
    });

    //tooltip button close Menu
    $('#menu_closed').tooltip({
        placement: "right"
    });

    //tooltip button close sidebar
    $('#sidebar_closed').tooltip({
        placement: "left"
    });

    //tooltip button toggle contact
    $('#tabs').tooltip({
        placement: "right"
    });

    //tooltip button toggle contact
    $('#tabs_resp').tooltip({
        placement: "right"
    });

    //tooltip button close Contact
    $('#contact_closed').tooltip({
        placement: "left"
    });



    /* ---------------------------------------------------------------------- */
    /* ----------------------- widget Latest galleries -------------------------- */
    /* ---------------------------------------------------------------------- */

    // var animation_style_h3 = 'slideInLeft';
    //  var animation_style_p = 'slideInRight';

    var animation_style_h3 = 'fadeInLeft';
    var animation_style_p = 'fadeInRight';
    var animation_style_btn = 'bounceInUp';


    //carousel3-1
    $('.flexslider-sidebar').flexslider({
        animationLoop: true,
        animation: "slide",
        controlNav: false,
        directionNav: true,
        nextText: "<i class='fa fa-angle-right'></i>",
        prevText: "<i class='fa fa-angle-left'></i>",
        direction: "horizontal",
        slideshowSpeed: 8000,
        animationSpeed: 1400,
        pauseOnHover: true,
        randomize: false,
        smoothHeight: true,
        keyboardNav: true,
        controlsContainer: '.flexslider .flexslider-sidebar',
    });


    /* ---------------------------------------------------------------------- */
    /* ------------------------- social header ------------------------------ */
    /* ---------------------------------------------------------------------- */


    $('.social-ul li').hover(function () {

        var tab_name = $(this).find('i');
        var animation_style = 'fadeInUp';

        $(tab_name).addClass('animated ' + animation_style);
        $(tab_name).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(tab_name).removeClass('animated ' + animation_style);
        });

    }, function () {

        var tab_name1 = $(this).find('i');
        var animation_style1 = 'fadeInDown';

        $(tab_name1).addClass('animated ' + animation_style1);
        $(tab_name1).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
            $(tab_name1).removeClass('animated ' + animation_style1);
        });


    });


    /* ---------------------------------------------------------------------- */
    /* ----------------------------- Portfolio ------------------------------ */
    /* ---------------------------------------------------------------------- */

    /* filter open close */

    $("#genre-filter li.label_filter").click(function () {
        $("#genre-filter li").not(".label_filter").toggle(300);
        return false;
    });


    /* filterList portfolio */
    var filterList = {
        init: function () {
            // MixItUp plugin
            // http://mixitup.io
            $('#portfoliolist').mixitup({
                targetSelector: '.portfolio',
                filterSelector: '.filter',
                effects: ['fade'],
                easing: 'snap',
                // call the hover effect
                onMixEnd: filterList.hoverEffect()
            });
        },
        hoverEffect: function () {

            // Simple parallax effect
            /*$('#portfoliolist .portfolio').hover(
             function() {
             $(this).find('.label').stop().animate({bottom: 0}, 200);
             $(this).find('img').stop().animate({top: -30}, 500);
             },
             function() {
             $(this).find('.label').stop().animate({bottom: -40}, 200);
             $(this).find('img').stop().animate({top: 0}, 300);
             }
             );*/

        }

    };

    // Run the show!
    filterList.init();

    /* ---------------------------------------------------------------------- */
    /* ----------------------------- prettyPhoto ---------------------------- */
    /* ---------------------------------------------------------------------- */

    $("a[rel^='portfolio'], a[data-gal^='portfolio']").prettyPhoto({
        animation_speed: 'fast', /* fast/slow/normal */
        social_tools: '',
        theme: 'pp_default',
        horizontal_padding: 5,
        deeplinking: false
    });

    /* ---------------------------------------------------------------------- */
    /* -------------------------- Contact Form ------------------------------ */
    /* ---------------------------------------------------------------------- */

    // Needed variables
    var $contactform = $('#contactform');
    var $success = ' Your message has been sent. Thank you!';
    var response = '';

    $('#contactform').submit(function () {
        $.ajax({
            type: "POST",
            url: "php/contact.php",
            data: $(this).serialize(),
            success: function (msg) {

                var msg_error = msg.split(",");
                var output_error = '';

                if (msg_error.indexOf('error-message') != -1) {
                    $("#contact-message").addClass("has-error");
                    $("#contact-message").removeClass("has-success");
                    output_error = 'Please enter your message.';
                } else {
                    $("#contact-message").addClass("has-success");
                    $("#contact-message").removeClass("has-error");
                }


                if (msg_error.indexOf('error-email') != -1) {
                    $("#contact-email").addClass("has-error");
                    $("#contact-email").removeClass("has-success");
                    output_error = 'Please enter valid e-mail.';
                } else {
                    $("#contact-email").addClass("has-success");
                    $("#contact-email").removeClass("has-error");
                }

                if (msg_error.indexOf('error-name') != -1) {
                    $("#contact-name").addClass("has-error");
                    $("#contact-name").removeClass("has-success");
                    output_error = 'Please enter your name.';
                } else {
                    $("#contact-name").addClass("has-success");
                    $("#contact-name").removeClass("has-error");
                }

                if (msg == 'success') {

                    response = '<div class="alert alert-success success-send">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            '<i class="glyphicon glyphicon-ok" style="margin-right: 5px;"></i> ' + $success
                            + '</div>';

                    $(".name-contact").val('');
                    $(".email-contact").val('');
                    $(".subject-contact").val('');
                    $(".message-contact").val('');

                    $("#contact-name").removeClass("has-success");
                    $("#contact-email").removeClass("has-success");
                    $("#contact-subject").removeClass("has-success");
                    $("#contact-message").removeClass("has-success");

                } else {

                    response = '<div class="alert alert-danger error-send">' +
                            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
                            '<i class="glyphicon glyphicon-remove" style="margin-right: 5px;"></i> ' + output_error
                            + '</div>';

                }
                // Hide any previous response text
                $(".error-send,.success-send").remove();

                // Show response message
                $contactform.prepend(response);
            }
        });

        return false;
    });


    /* ---------------------------------------------------------------------- */
    /* ----------------------- jQuery Flexslider ---------------------------- */
    /* ---------------------------------------------------------------------- */

    /* slider blog post */
    $('.flexslider-blog').flexslider({
        animationLoop: true,
        animation: "slide", /* fade */
        controlNav: true,
        directionNav: true,
        nextText: "<i class='fa fa-angle-right'></i>",
        prevText: "<i class='fa fa-angle-left'></i>",
        direction: "horizontal",
        slideshowSpeed: 8000,
        animationSpeed: 1400,
        pauseOnHover: true,
        randomize: false,
        smoothHeight: true,
        keyboardNav: true,
        controlsContainer: '.flexslider-blog'
    });

    /* carousel blog */
    $(".carousel-wrap").show();
    $('.carousel-wrap').flexslider({
        slideshow: true,
        slideshowSpeed: 3000,
        mousewheel: false,
        keyboard: true,
        controlNav: false,
        directionNav: true,
        nextText: "<i class='fa fa-angle-right'></i>",
        prevText: "<i class='fa fa-angle-left'></i>",
        animation: "slide",
        itemWidth: 290,
        itemMargin: 1,
        minItems: 1,
        maxItems: 4,
        move: 1,
    });


    $('.flexslider-grid').flexslider({
        animationLoop: true,
        animation: "fade",
        controlNav: true,
        directionNav: true,
        nextText: "<i class='fa fa-angle-right'></i>",
        prevText: "<i class='fa fa-angle-left'></i>",
        slideshowSpeed: 7000,
        animationSpeed: 600,
        randomize: false,
        pauseOnHover: true,
        slideshow: true,
        smoothHeight: true,
        keyboardNav: true,
        controlsContainer: '.flexslider-grid'
    });

    //related-posts
    $('.carousel_related_post').flexslider({
        animationLoop: true,
        animation: "slide",
        controlNav: false,
        directionNav: true,
        controlsContainer: "#nav_carousel_related_post",
        itemWidth: 193,
        itemMargin: 20,
        minItems: 1,
        maxItems: 3,
        nextText: "",
        prevText: ""

    });

    //latest-articles
    $('.carousel_latest_articles').flexslider({
        animationLoop: true,
        animation: "slide",
        controlNav: false,
        directionNav: true,
        controlsContainer: "#nav_carousel_latest_articles",
        itemWidth: 193,
        itemMargin: 20,
        minItems: 1,
        maxItems: 3,
        nextText: "",
        prevText: ""

    });


    /* ---------------------------------------------------------------------- */
    /* --------------------------- Menu header ------------------------------ */
    /* ---------------------------------------------------------------------- */

    $('ul.vintage > li:has(ul)').addClass("has-sub");

    $('ul.vintage > li > a').click(function () {

        var checkElement = $(this).next();

        $('ul.vintage li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('ul.vintage > li > a').removeClass('menu-open');
        $(this).closest('li').find('a').addClass('menu-open');

        $(this).css('text-decoration', 'none');

        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            $(this).closest('li').find('a').removeClass('menu-open');
            checkElement.slideUp('normal');
        }

        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('ul.vintage ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }

        if (checkElement.is('ul')) {
            return false;
        } else {
            return true;
        }

    });

    /* ---------------------------------------------------------------------- */
    /* --------------------------- Map Contact ------------------------------ */
    /* ---------------------------------------------------------------------- */

    if ($('#map_canvas').length > 0) {
        initializes();
    }


    function initializes() {
        var contentString = '<div id="gmaps">' +
                '<p id="tabs" class="tab_close" data-toggle="tooltip" data-original-title="Contact"><i class="fa fa-book"></i></p><p id="tabs_resp" class="tab_close" data-toggle="tooltip" data-original-title="Contact"><i class="fa fa-book"></i></p>' +
                '<p style="font-family: Arial, Helvetica, sans-serif;">44 E. 8th Street Suite 300 Holland.<br /><br />Phone: +61 3 8376 6284</p>' +
                '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });


        var mapOptions = {
            zoom: 14,
            center: new google.maps.LatLng(-37.815921, 144.964085),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(-37.815921, 144.964085),
            map: map,
            title: '44 E. 8th Street Suite 300 Holland.',
            icon: 'images/map-marker.png'
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });

    }

    $("#tabs").click(function () {
        $("#contentContact").stop();
        if ($(this).hasClass('tab_close')) {
            $(this).removeClass('tab_close');
            var left = 275;
            if ($("#header").hasClass("slide")) {
                left = 0;
            }
            $("#contentContact").animate({left: left + 'px'}, 1500, 'easeOutCubic');
        } else {
            $(this).addClass('tab_close');
            $("#contentContact").animate({left: '-350px'}, 1500, 'easeOutCubic');
        }
    });


    $("#tabs_resp").click(function () {
        if ($(this).hasClass('tab_close')) {

            $(this).removeClass('tab_close');
            $("#contentContact").animate({left: '0px'}, 1500, 'easeOutCubic');

        } else {

            $(this).addClass('tab_close');
            $("#contentContact").animate({left: '-350px'}, 1500, 'easeOutCubic');
        }
    });

    $("#contact_closed").click(function () {
        if ($("#tabs_resp").hasClass('tab_close')) {

            $("#tabs_resp").removeClass('tab_close');
            $("#contentContact").animate({left: '0px'}, 1500, 'easeOutCubic');

        } else {

            $("#tabs_resp").addClass('tab_close');
            $("#contentContact").animate({left: '-350px'}, 1500, 'easeOutCubic');
        }
    });

    /* ---------------------------------------------------------------------- */
    /* --------------------------- Menu mobile ------------------------------ */
    /* ---------------------------------------------------------------------- */

    $('ul#menu_mobile li.menu-item-has-children:has(ul)').addClass("has-sub");

    $('ul#menu_mobile li.menu-item-has-children > a').click(function () {

        var checkElement = $(this).next();

        $('ul#menu_mobile li.menu-item-has-children').removeClass('active');
        $(this).closest('li').addClass('active');
        $('ul#menu_mobile li.menu-item-has-children').removeClass('menu-open');
        $(this).closest('li').addClass('menu-open');

        $(this).css('text-decoration', 'none');

        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            $(this).closest('li').removeClass('menu-open');
            checkElement.slideUp('normal');
        }

        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            //$('ul#menu_mobile ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }

        if (checkElement.is('ul')) {
            return false;
        } else {
            return true;
        }

    });
    var ajax_sent = false;
    $(window).scroll(function () {
        if (ajax_sent === true)
            return;
        if ($(this).scrollTop() + $(this).height() >= $(document).height() - 150) {
            if ($('[data-toggle="inf_scroll"').length) {
                ajax_sent = true;
                var el = $('[data-toggle="inf_scroll"');
                var last_id = Number(el.attr('data-last_id'));
                var limit = el.attr('data-limit');
                if (!limit || limit === undefined)
                    limit = 10;
                limit = Number(limit);
                el.append('<div class="col-md-3 col-sm-6" id="special_loading" style="opacity: 0.5"><article><div class="post-thumb"><a href="#" class="image-link"><img src="http://placehold.it/333x222&text=Loading... "/></a></div><div class="post-body"><h3 class="post-title"><center>Loading <i class="fa fa-refresh fa-spin"></i></center></h3></div></article></div>');
                setTimeout(function () {
                    $.get(ajax_url, {action: 'get_more_followers', limit: limit, last_id: last_id}, function (data) {
                        ajax_sent = false;
                        $('#special_loading').remove();
                        el.attr('data-last_id', last_id + limit);
                        el.append(data);
                    });
                }, 1000);
            }
            if ($('.masonry_load_more').length) {
                setTimeout(function () {
                    $('.masonry_load_more').click();
                }, 800);
            }
        }
    });

    $('#go-top-button2').click(function (event) {
        event.preventDefault();
        $('#sidebar-wrapper').animate({scrollTop: 0}, 600);
    });

    var sidebar_scroll_top_shown = false;
    $('#sidebar-wrapper').scroll(function () {
        if ($(this).scrollTop() > 500) {
            if (!sidebar_scroll_top_shown) {
                $('#go-top-button2').stop().addClass("visible");
                sidebar_scroll_top_shown = true;
            }
        } else {
            if (sidebar_scroll_top_shown) {
                $('#go-top-button2').stop().removeClass("visible");
                sidebar_scroll_top_shown = false;
            }
        }
    });


    $('.masonry_load_more').click(function () {
        if (ajax_sent === true)
            return;
        ajax_sent = true;
        var el = $(this);
        var text_container = el.find('#load_more_text');
        var original_text = text_container.html();
        text_container.html('Loading...');
        setTimeout(function () {
            $.get(ajax_url, {action: 'more_home_posts'}, function (data) {
                ajax_sent = false;
                text_container.html(original_text);
                $('.js-masonry').append(data).ready(function () {
                    $('.js-masonry').masonry('reloadItems').masonry('layout');
                    $("div.bhoechie-tab-menu>div.list-group>a[data-clicked=\"1\"]").click();
                });
            });
        }, 500);
    });

    $('#profile-panel-toggle').click(function () {
        $('#header').toggleClass('slide');
        $(this).toggleClass('slide', function () {
            if ($(this).hasClass('slide')) {
                $('.bhoechie-tab-menu').css('margin-left', '-22%').addClass('transition_400');
                $('#all_posts_div').css({
                    'width': '72%',
                    'margin-left': '-16%'
                }).addClass('transition_400');

            } else {
                $('.bhoechie-tab-menu').css('margin-left', 'auto').addClass('transition_400');
                $('#all_posts_div').css({
                    'width': '50%',
                    'margin-left': 'auto'
                }).addClass('transition_400');
            }
        });
        $('div#content-wrapper,.top-menu').toggleClass('full_width');
        if ($('.js-masonry').length) {
            setTimeout(function () {
                $('.js-masonry').masonry('layout');
                if ($('#profile-panel-toggle').hasClass('slide')) {
                    var height = "181px";
                    $('span.text-content').css({
                        'height': height,
                        'top': '44px'
                    }).find('i').addClass('fa-2x');
                    $('span.text-content').find('i.fa-thumb-tack').css('margin-left', '2%');
                    $('span.text-content').find('i.fa-star').css('margin-left', '9%');
                    $('span.text-content').find('i.fa-arrow-down').css('margin-left', '-6%');
                } else {
                    var height = "131px";
                    $('span.text-content').css({
                        'height': height,
                        'top': '60px',
                    }).find('i').removeClass('fa-2x');
                    $('span.text-content').find('i.fa-thumb-tack').css('margin-left', '11%');
                    $('span.text-content').find('i.fa-star').css('margin-left', '18%');
                    $('span.text-content').find('i.fa-arrow-down').css('margin-left', '3%');
                }
                $('.img-list').css('height', height);
                $('.post_video_size').css('height', height);
            }, 420);
        }
        if ($("#contentContact").length) {
            $("#contentContact").stop();
        }
        $(this).find('.fa').removeClass("fa-chevron-left fa-chevron-right");
        //change the chevron
        if ($('#header').hasClass('slide')) {
            $(this).find('.fa').addClass("fa-chevron-right");
            $('#header').getNiceScroll().remove();
            if ($('#tabs').length && !$('#tabs').hasClass('tab_close')) {
                $("#contentContact").animate({left: '0px'}, 400, 'easeOutCubic');
            }
        } else {
            $(this).find('.fa').addClass("fa-chevron-left");
            setTimeout(function () {
                $('#header').niceScroll({
                    touchbehavior: false,
                    scrollspeed: 60,
                    mousescrollstep: 40,
                    cursorwidth: 10,
                    cursorborder: 0,
                    autohidemode: false,
                    zindex: 99999999,
                    horizrailenabled: false,
                    cursorborderradius: 15,
                    cursorcolor: "#5f5f5f"
                });
            }, 410);
            if ($('#tabs').length && !$('#tabs').hasClass('tab_close')) {
                $("#contentContact").animate({left: '275px'}, 400, 'easeOutCubic');
            }
        }
    });

    $('#menu_layout_toggle_button').click(function () {
        $('.menu_content_wrapper,#btn_sidebar_wrapper,#profile-panel-toggle').toggleClass('inverse');
        $.cookie('menu_layout', null);
        if ($('.menu_content_wrapper').hasClass('inverse')) {
            $.cookie('menu_layout', true);
        }
    });

    $('#button-reset a').click(function (e) {
        e.preventDefault();
        $.cookie('menu_layout', null);
        $('.menu_content_wrapper,#btn_sidebar_wrapper,#profile-panel-toggle').removeClass('inverse');
    });

    /* User Options */
    $('.options_toggle').click(function () {
        $('.options_pane').toggleClass('active');
    });
    if ($('.js-masonry').length) {
        var w = window.innerWidth;
        if (w > 2000) {
            $('.item').css('width', '25%');
            $('.js-masonry').masonry('layout');
        }
        if (w < 1200) {
            $('.item').css('width', '100%');
            $('.js-masonry').masonry('layout');
        }
    }
    $('#btn_sidebar_wrapper').click();
    setTimeout(function () {
        $('.js-masonry').masonry('layout');
    }, 1000);
    $.get(ajax_url, {action: 'more_home_posts'}, function (data) {
        $('.default_content').html(data);
        $('.js-masonry').masonry({"columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s"}).masonry('reloadItems').masonry('layout');
        $("div.bhoechie-tab-menu>div.list-group>a[data-clicked=\"1\"]").click();
    });
    $('#images_newest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#groups_latest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#lightSlider1').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#temp2_lightSlider1').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target === '#groups_featured') {
            if (!window.gf_slider) {
                window.gf_slider = $('#groups_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.gf_slider.refresh();
        }
        if (target === '#groups_popular') {
            if (!window.gp_slider) {
                window.gp_slider = $('#groups_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.gp_slider.refresh();
        }
        if (target === '#groups_all') {
            if (!window.ga_slider) {
                window.ga_slider = $('#groups_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ga_slider.refresh();
        }

        if (target === '#images_featured') {
            if (!window.if_slider) {
                window.if_slider = $('#images_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.if_slider.refresh();
        }
        if (target === '#images_popular') {
            if (!window.ip_slider) {
                window.ip_slider = $('#images_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ip_slider.refresh();
        }
        if (target === '#images_all') {
            if (!window.ia_slider) {
                window.ia_slider = $('#images_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ia_slider.refresh();
        }
    });
}).on('click', '.img_options', function () {
    var img = $(this).attr('src');
    $('#content_data').html('<img class="img img-thumbnail border-radius" src="' + img + '" style="width:100%;height: 360px;"/>');
    $('#post_link_attr').attr('href', 'post.php?post_type=image');
    $("#myModal").modal();
}).on('click', '.video_options', function () {
    var video = $(this).attr('data-video');
    $('#content_data').html('<iframe src="https://www.youtube.com/embed/' + video + '" style="width:100%;height: 360px; class="border-radius"></iframe>');
    $('#post_link_attr').attr('href', 'post.php?post_type=video');
    $("#myModal").modal();
}); // close

$(document).on('click', 'pop', function () {
    var value = $(this).val();
    $('select').find('option[value="' + value + '"]').attr('selected', true);
});