$(document).ready(function () {
// Tabs JS
    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $("div.bhoechie-tab-menu>div.list-group>a").attr('data-clicked', 0);
        $(this).attr('data-clicked', 1);

        // Get Post Type
        var post_type = $(this).attr('data-post_type');

        // Media Posts
        if (post_type === 'media') {
            $('.item[data-type]').show();
            $('#media_preview').show();
            $('.item[data-type!="' + post_type + '"]').hide();
            $('.js-masonry').masonry({
                columnWidth: '.item[data-type="' + post_type + '"]'
            }).masonry('layout');

            // Only Mine Posts    
        } else if (post_type === 'only_mine') {
            $('.item[data-type]').show();
            $('.only_mine_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loading.gif"/></center>');
            $.get(ajax_url, {action: 'only_mine_posts'}, function (data) {
                $('.only_mine_content').html(data);
                $('.js-masonry').masonry('reloadItems').masonry('layout');
            });

            // Members    
        } else if (post_type === 'member') {
            $('.item[data-type]').show();
            $('.item[data-type!="' + post_type + '"]').hide();
            $('.show_online_members').html('<center><img style="margin: 30px 0;" src="images/ajax-loading.gif"/></center>');
            $.get(ajax_url, {action: 'get_online_members'}, function (data) {
                $('.show_online_members').html(data);
                $('.js-masonry').masonry('reloadItems').masonry('layout');
            });

            // Groups    
        } else if (post_type === 'groups') {
            $('.item[data-type]').show();
            $('.all_groups_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loading.gif"/></center>');
            $.get(ajax_url, {action: 'get_all_groups'}, function (data) {
                $('.all_groups_content').html(data);
                $('.js-masonry').masonry('reloadItems').masonry('layout');
            });

        } else {
            $('.item[data-type]').show();
            $('.js-masonry').masonry('layout');
            $('#media_preview').hide();
        }
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active").find('.js-masonry').masonry('layout');
    });
});