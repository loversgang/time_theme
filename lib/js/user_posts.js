// Text Post
$(document).on('click', '#post_text', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_text_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Photo Post
$(document).on('click', '#post_photo', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_photo_html'}, function (data) {
        $('#post_content').html(data);
        $('.fileinput').fileinput();
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Link Post
$(document).on('click', '#post_link', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_link_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Video Post
$(document).on('click', '#post_video', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_video_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Audio Post
$(document).on('click', '#post_audio', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_audio_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Event Post
$(document).on('click', '#post_event', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_event_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
// Event Post
$(document).on('click', '#post_collection', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_collection_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
        }, 200);
    });
});
$(document).on('click', '#upload_photo', function () {
    $(".inputFile").click();
});
$(document).on('change', '.inputFile', function () {
    $('#f_name').html($(this).val().replace(/C:\\fakepath\\/i, ''));
});
$(document).on('submit', '#uploadForm', (function (e) {
    e.preventDefault();
    $("#targetLayer").html('<center><img style="margin: 65px 0;" src="images/ajax-loader.gif"/></center>');
    $.ajax({
        url: "upload.php",
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data)
        {
            $("#targetLayer").html(data);
        },
        error: function ()
        {
        }
    });
}));
$(document).on('click', '#embed_youtube_video', function () {
    var url = $('#videoUrl').val();
    var video = '<iframe class="img img-thumbnail" src="' + url + '" style="width:100%"></iframe>';
    $("#youtube_frame").html('<center><img style="margin: 65px 15px;" src="images/ajax-loader.gif"/></center>');
    setTimeout(function () {
        $("#youtube_frame").html(video);
    }, 1000);
});
$(document).on('click', '#embed_soundcloud_audio', function () {
    var url = $('#audioUrl').val();
    var audio = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/' + url + '&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true"></iframe>';
    $("#soundcloud_frame").html('<center><img style="margin: 65px 15px;" src="images/ajax-loader.gif"/></center>');
    setTimeout(function () {
        $("#soundcloud_frame").html(audio);
    }, 1000);
});
$(document).on('click', '#get_location_html', function () {
    $('#location_html').css({
        'background-color': '#eee',
        'margin-top': '10px'
    });
    $('#location_input').slideToggle(100);
});
$(document).on('click', '#get_url_info', function () {
    var url = $('#url').val();
    extractURL(url);
});
function extractURL(url) {
    $.ajax({
        url: ajax_url,
        data: {
            action: 'extract_url',
            url: url
        },
        type: "POST",
        beforeSend: function () {
            $("#url").css("background", "#FFF url(images/ajax-loader.gif) no-repeat right center");
        },
        success: function (responseData) {
            $("#output").html(responseData);
            $("#output").slideDown();
            $("#url").css("background", "");
        }
    });
}
function navigateImage(urlAry, nav) {
    var urlArrayLength = urlAry.length;
    var index = $('#index').val();
    $('#prev-extract').prop('disabled', false);
    $('#next-extract').prop('disabled', false);
    if (nav === 'prev') {
        if (index !== 0) {
            var prev_index = (parseInt(index) - 1);
            $('#index').val(prev_index);
            $(".image-extract img").attr('src', urlAry[prev_index]);
            if (prev_index === 0)
                $('#prev-extract').prop('disabled', true);
        }
    }
    if (nav === 'next') {
        if (index < urlArrayLength - 1) {
            var next_index = (parseInt(index) + 1);
            $('#index').val(next_index);
            $(".image-extract img").attr('src', urlAry[next_index]);
            if (next_index >= urlArrayLength - 1)
                $('#next-extract').prop('disabled', true);
        }
    }
}

$(document).on('click', '.later', function () {
    toastr.remove();
    toastr.info('Functionality will add later', 'Info');
});
$(document).on('click', '.submit_content', function () {
    var data_array = {};
    data_array.text_content = $('#text_content').val();
    data_array.text_location = $('#text_location').val();
    data_array.collection = $('#collection').val();
    data_array.content_privacy = $('#content_privacy').val();
    $.post(ajax_url, {action: 'upload_status', data_array: data_array}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});