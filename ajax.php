<?php
//gatekeeper();
include dirname(dirname(__DIR__)) . "/engine/start.php";
include 'functions.php';
include 'simple_html_dom.php';
$logged_user = elgg_get_logged_in_user_entity();
// Get Active Members
$active_members = find_active_users();

// Get Default Collections
$default_collections = get_default_access();

// Get Logged User Collections
$collections = get_user_access_collections($logged_user->guid);

// Get Circles/Cliques
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
if (isset($_GET['action']) && $_GET['action']) {
    extract($_GET);
    if ($action === 'more_home_posts') {
        $entities = elgg_get_entities(array(
            'limit' => FALSE,
        ));
        ?>
        <?php foreach ($entities as $entity) { ?>
            <b>
                <?php
                $subtype = get_subtype_from_id($entity->subtype);
                ?>
            </b>
            <?php
            if ($entity->type == 'group' || $subtype == 'file' || $subtype == 'text_content') {
                $owner = get_user_entity_as_row($entity->owner_guid);
                if ($entity->type == 'group') {
                    $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
                    ?>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                                <br/>
                                <div class="post_content" style="padding:5px">
                                    <?php echo substr($entity->description, 0, 200); ?>..
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } elseif ($subtype == 'file') {
                    $line = 'added new photo <a href="' . elgg_get_site_url() . 'file/view/' . $entity->guid . '/' . str_replace(' ', '-', $entity->title) . '">' . $entity->title . '</a>';
                    ?>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                                <br/>
                                <div class="post_content" style="padding:5px">
                                    <?php echo substr($entity->description, 0, 200); ?>..
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } elseif ($subtype == 'text_content') {
                    $post = maybe_unserialize($entity->description);
                    $collection = get_access_collection($post['collection']);
                    $post_location = $collection->name;
                    $line = 'updated status';
                    ?>
                    <div class="item" data-type="text" style="position: absolute; left: 0px; top: 0px;">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px">
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    <?php echo substr($post['text_content'], 0, 200); ?>..
                                    <br/>
                                    <br/>
                                    <span class="location">
                                        <?php echo $post['text_location']; ?> . <?php echo $post_location; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    $line = 'Text';
                }
            } else {
                continue;
            }
        }
    }
    if ($action === 'only_mine_posts') {
        $entities = elgg_get_entities(array(
            'limit' => FALSE,
            'owner_guid' => $logged_user->guid,
        ));
        ?>
        <?php foreach ($entities as $entity) { ?>
            <b>
                <?php
                $subtype = get_subtype_from_id($entity->subtype);
                ?>
            </b>
            <?php
            if ($entity->type == 'group' || $subtype == 'file' || $subtype == 'text_content') {
                $owner = get_user_entity_as_row($entity->owner_guid);
                if ($entity->type == 'group') {
                    $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
                    ?>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                                <br/>
                                <div class="post_content" style="padding:5px">
                                    <?php echo substr($entity->description, 0, 200); ?>..
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } elseif ($subtype == 'file') {
                    $line = 'added new photo <a href="' . elgg_get_site_url() . 'file/view/' . $entity->guid . '/' . str_replace(' ', '-', $entity->title) . '">' . $entity->title . '</a>';
                    ?>
                    <div class="item" data-type="media">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                                <div class="img-list">
                                    <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                                    <span class="text-content">
                                        <span>
                                            <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                            <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                            <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                            <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                            <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                        </span>
                                    </span>
                                </div>
                                <br/>
                                <div class="post_content" style="padding:5px">
                                    <?php echo substr($entity->description, 0, 200); ?>..
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } elseif ($subtype == 'text_content') {
                    $post = maybe_unserialize($entity->description);
                    $collection = get_access_collection($post['content_privacy']);
                    $post_location = $collection->name;
                    $line = 'updated status';
                    ?>
                    <div class="item" data-type="text" style="position: absolute; left: 0px; top: 0px;">
                        <div class="row item_content">
                            <div class="col-md-2" style="width:65px">
                                <img src="images/cover-3.jpg" class="img img-circle" style="height:35px">
                            </div>
                            <div class="col-md-8" style="margin-left: -20px;">
                                <i>
                                    <b>
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                            <?php echo $owner->name; ?>
                                        </a>
                                    </b>
                                    <font style="color:#999">
                                    <?php echo $line ?>
                                    <br/>
                                    <?php echo time_stamp($entity->time_created); ?>
                                    </font>
                                </i>
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    <?php echo substr($post['text_content'], 0, 200); ?>..
                                    <br/>
                                    <br/>
                                    <span class="location">
                                        <?php echo $post['text_location']; ?> . <?php echo $post_location; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else {
                    $line = 'Text';
                }
            } else {
                continue;
            }
        }
    }
    if ($action === 'get_all_groups') {
        $entities = elgg_get_entities(array(
            'limit' => FALSE,
            'type' => 'group',
        ));
        ?>
        <?php foreach ($entities as $entity) { ?>
            <?php
            $owner = get_user_entity_as_row($entity->owner_guid);
            $line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
            ?>
            <div class="item" data-type="media">
                <div class="row item_content">
                    <div class="col-md-2" style="width:65px">
                        <img src="<?php echo $entity->getIconURL('medium'); ?>" class="img img-circle" style="height:35px"/>
                    </div>
                    <div class="col-md-8" style="margin-left: -20px;">
                        <i>
                            <b>
                                <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $owner->username; ?>">
                                    <?php echo $owner->name; ?>
                                </a>
                            </b>
                            <font style="color:#999">
                            <?php echo $line ?>
                            <br/>
                            <?php echo time_stamp($entity->time_created); ?>
                            </font>
                        </i>
                    </div>
                    <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                        <div class="img-list">
                            <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                            <span class="text-content">
                                <span>
                                    <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                                    <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                                    <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                                    <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                                    <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                                </span>
                            </span>
                        </div>
                        <br/>
                        <div class="post_content" style="padding:5px">
                            <?php echo substr($entity->description, 0, 200); ?>..
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    if ($action === 'get_online_members') {
        ?>
        <?php foreach ($active_members as $member) { ?>
            <div class="item" data-type="member">
                <div class="row item_content">
                    <div class="col-md-12" style="padding:0px;margin-top: -5px">
                        <img class="img_options post_video_size" data-video="XZ4X1wcZ1GE" src="<?php echo $member->getIconURL('large'); ?>" />
                    </div>
                    <div class="col-md-12 photo_comments">
                        <div class="col-md-2 comments_col_width">
                            <img src="<?php echo $member->getIconURL('medium'); ?>" class="img img-circle" style="height:35px;margin-top: -30px;"/>
                        </div>
                        <div class="col-md-9 members_details">
                            <div class="post_user_detail">
                                <span class="p_bold"><?php echo $member->name; ?></span>
                                <span class="pull-right" style="color:#9CD759">
                                    <i class="fa fa-circle"></i> Online
                                </span>
                                <br/>
                                <span class="p_bold">@<?php echo $member->username; ?></span><br/>
                                <span class="photo_content"><?php echo $member->briefdescription ? $member->briefdescription : '' ?></span><br />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="color:#FF7101;padding: 0px;margin-left: -8px;">
                        <div class="col-md-2">
                            <i class="fa fa-inbox fa fa-2x"></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-cog  fa fa-2x"></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-plus  fa fa-2x"></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-eye fa fa-2x"></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-video-camera fa fa-2x"></i>
                        </div>
                        <div class="col-md-2">
                            <i class="fa fa-gift fa fa-2x"></i>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
    }
}
?>
<?php
if (isset($_POST['action']) && $_POST['action']) {
    extract($_POST);
    if ($action == 'get_post_text_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea id="text_content" class="form-control border-radius border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="text_location" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <?php if ($collections) { ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label name="select_collection">Add To Collection</label>
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-database"></i></div>
                                <select id="collection" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                                    <?php foreach ($collections as $collection) { ?>
                                        <option value="<?php echo $collection->id; ?>"><?php echo $collection->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select id="content_privacy" class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <?php foreach ($circles as $circle) { ?>
                                    <option value="<?php echo $circle->guid ?>"><?php echo $circle->title ?></option>
                                <?php } ?>
                            </select>
                            <div style="cursor: pointer" class="submit_content input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_photo_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12">
                    <center>
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                            <div>
                                <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </center>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Add Photo To Collection</label>
                        <div class="input-group">
                            <input placeholder="Enter Title" type="text" class="form-control border-radius" />
                            <div class="input-group-addon border-radius black-green-button colors-color">Create Collection</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_link_html') {
        ?>
        <div class="row-fluid">
            <form class="form-inline">
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Grab Link Info</label>
                        <div class="input-group">
                            <input id="url" placeholder="Enter URL" type="text" class="form-control border-radius" />
                            <div id="get_url_info" class="input-group-addon border-radius black-green-button colors-color">Fetch</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="output"></div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select id="content_privacy" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option value="2">Public</option>
                                <option value="0">Private</option>
                                <option value="-2">Friends</option>
                                <?php foreach ($collections as $collection) { ?>
                                    <option value="<?php echo $collection->id; ?>"><?php echo $collection->name; ?></option>
                                <?php } ?>
                            </select>
                            <div style="cursor: pointer" class="submit_content input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_audio_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input type="text" class="form-control border-radius" id="audioUrl" name="audioUrl" placeholder="Eg. 101276036" value="101276036"/>
                        <div id="embed_soundcloud_audio" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="soundcloud_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">

                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>                         
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">Post</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Audio To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }
    if ($action == 'get_post_video_html') {
        ?>
        <div class="row-fluid">
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Enter Track ID</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input value="https://www.youtube.com/embed/E6KwXYmMiak" type="text" class="form-control border-radius" id="videoUrl" name="videoUrl" placeholder="Youtube Video URL"/>
                        <div id="embed_youtube_video" class="input-group-addon border-radius  black-green-button colors-color">Go</div>
                    </div>
                </div>
            </div>
            <div id="col-md-12" style="margin-top:10px">
                <div id="youtube_frame"></div>
            </div>
            <form>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <div class="input-group">
                            <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                            <div style="cursor:pointer" class="input-group-addon border-radius  black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div id="location_input" style="display: none">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                                <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                                <div class="input-group-addon border-radius  black-green-button colors-color">Find</div>
                            </div>
                        </div>
                        <div id="location_html">
                            <center><img src="images/splash-screen.png"/></center>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top:10px">
                    <div class="form-group">
                        <label class="control-label">Privacy</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                            <select class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                <option>Public</option>
                                <option>Friends</option>
                            </select>
                            <div class="input-group-addon border-radius black-green-button colors-color">POST</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label name="select_collection">Add Video To Collection</label>
                    <div class="form-group">
                        <select class="form-control border-radius">
                            <option>Collection Name</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <?php
    }

    if ($action == 'get_post_event_html') {
        ?>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <input placeholder="Enter Event Title" type="text" class="form-control border-radius" />
            </div>
            <div class="form-group">
                <label class="control-label">Enter Event Date</label>
                <input type="date" class="form-control border-radius" />
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <div class="input-group">
                    <textarea class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                    <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="location_input" style="display: none">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input id="pac-input" placeholder="Type Location" type="text" class="form-control border-radius" />
                        <div class="input-group-addon border-radius black-green-button colors-color">Find</div>
                    </div>
                </div>
                <div id="location_html">
                    <center><img src="images/splash-screen.png"/></center>
                </div><br />
            </div>
            <div class="form-group">
                <textarea placeholder="Enter Event Description" type="text" class="form-control border-radius" /></textarea>
            </div>
            <label name="select_collection">Add Event To Collection</label>
            <div class="form-group">
                <select class="form-control border-radius">
                    <option>Collection Name</option>
                </select>
            </div>
        </div>

    <?php } ?>
    <?php if ($action == 'get_post_collection_html') { ?>

        <?php
    }
    if ($action == 'upload_status') {
        $data = new ElggObject();
        $data->subtype = "text_content";
        $data->title = 'text_content';
        $data->description = maybe_serialize($data_array);
        $data->access_id = $data_array['collection'] ? $data_array['collection'] : ACCESS_PUBLIC;
        $data->owner_guid = elgg_get_logged_in_user_guid();
        $data_id = $data->save();
        if ($data_id) {
            system_message("Status Updated Successfully!");
            forward($data_id->getURL());
        } else {
            register_error("Something Went Wrong!");
            forward(REFERER);
        }
    }

    if ($action == 'extract_url') {
        if (!empty($_POST["url"]) && filter_var($_POST["url"], FILTER_VALIDATE_URL)) {
            //extracting HTML content for the URL 
            $content = file_get_html($_POST["url"]);

            //Parsing Title 
            foreach ($content->find('title') as $element) {
                $title = $element->plaintext;
            }

            //Parsing Body Content
            foreach ($content->find('body') as $element) {
                $body_content = implode(' ', array_slice(explode(' ', trim($element->plaintext)), 0, 50));
            }

            $image_url = array();

            //Parse Site Images
            foreach ($content->find('img') as $element) {
                if (filter_var($element->src, FILTER_VALIDATE_URL)) {
                    list($width, $height) = getimagesize($element->src);
                    if ($width > 150 || $height > 150) {
                        $image_url[] = $element->src;
                    }
                }
            }
            $image_div = "";
            if (!empty($image_url[0])) {
                $image_div = "<div class='image-extract'>" .
                        "<input type='hidden' id='index' value='0'/>" .
                        "<img id='image_url' src='" . $image_url[0] . "' />";
                if (count($image_url) > 1) {
                    $image_div .= "<div>" .
                            "<input type='button' class='btnNav' id='prev-extract' onClick=navigateImage(" . json_encode($image_url) . ",'prev') disabled />" .
                            "<input type='button' class='btnNav' id='next-extract' target='_blank' onClick=navigateImage(" . json_encode($image_url) . ",'next') />" .
                            "</div>";
                }
                $image_div .="</div>";
            }

            $output = $image_div . "<div class='content-extract'>" .
                    "<h3><a href='" . $_POST["url"] . "' target='_blank'>" . $title . "</a></h3>" .
                    "<div>" . $body_content . "</div>" .
                    "</div>";
            echo $output;
        }
    }
    if ($action === 'delete_post') {
        $entity = get_entity($guid);
        if (($entity) && ($entity->canEdit())) {
            if ($entity->delete()) {
                system_message(elgg_echo('entity:delete:success', array($guid)));
            } else {
                register_error(elgg_echo('entity:delete:fail', array($guid)));
            }
        } else {
            register_error(elgg_echo('entity:delete:fail', array($guid)));
        }
    }

    // Save User Circles
    if ($action == 'save_user_circles') {
        $logged_user = elgg_get_logged_in_user_entity();
        $circles_value = 'circles_' . $logged_user->guid;
        $user_circles = ($logged_user->$circles_value) ? maybe_unserialize($logged_user->$circles_value) : array();
        $users_array = array();
        foreach ($circles as $circle) {
            if (array_key_exists($circle->guid, $user_circles)) {
                $users_array[$circle->guid] = $user_circles[$circle->guid];
            }
            if (in_array($circle->guid, $user_circles_array)) {
                if (!is_array($users_array[$circle->guid])) {
                    $users_array[$circle->guid] = array();
                }
                if (!in_array($user_id, $users_array[$circle->guid])) {
                    $users_array[$circle->guid][] = $user_id;
                }
            } else {
                if (in_array($user_id, $users_array[$circle->guid])) {
                    unset($users_array[$circle->guid][array_search($user_id, $users_array[$circle->guid])]);
                } else {
                    if (!$users_array[$circle->guid]) {
                        $users_array[$circle->guid] = '';
                    }
                }
            }
        }
        $data = maybe_serialize($users_array);
        create_metadata($logged_user->guid, $circles_value, $data, '', $logged_user->guid, ACCESS_PUBLIC);
        elgg_delete_orphaned_metastrings();
        $logged_user->save();
    }
}